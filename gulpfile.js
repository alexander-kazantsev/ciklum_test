/**
 * Created by Alexander Kazantsev on 9/29/15.
 */
var __files = require('./build_files'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    tmpCache = require('gulp-angular-templatecache'),
    connect = require('gulp-connect'),
    watch = require('gulp-watch');

gulp.task('scripts_dev', function () {
    return gulp.src(__files.APP_SCRIPTS)
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('./dist/assets/js'));
});

gulp.task('bower_scripts', function () {
    return gulp.src(__files.BOWER_COMPONENTS)
        .pipe(concat('bower-components.min.js'))
        .pipe(gulp.dest('./dist/assets/js'));
});

gulp.task('bower_css', function () {
    return gulp.src(__files.CSS)
        .pipe(concat('bower-components.min.css'))
        .pipe(gulp.dest('./dist/assets/css'))
});

gulp.task('css', function () {
    return gulp.src('./app/**/*.css')
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('./dist/assets/css'))
});

gulp.task('templates', function () {
    return gulp.src('./app/**/*.html')
        .pipe(tmpCache('templates.js', {standalone: true}))
        .pipe(gulp.dest('./dist/assets/js'))
});

gulp.task('fonts', function () {
    gulp.src(__files.FONTS)
        .pipe(gulp.dest('./dist/assets/fonts'))
});

gulp.task('copy_index', function () {
    return gulp.src('./app/index.html')
        .pipe(concat('index.html'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('watch', function () {

    gulp.watch('./dist/**/*.*', __reload);

    gulp.watch(['./app/**/*.js'], ['scripts_dev']);
    gulp.watch(['./app/**/*.html'], ['templates']);
    gulp.watch('./app/index.html', ['copy_index']);
});

gulp.task('connect', function () {
    return connect.server({
        root: ['dist'],
        port: 9000,
        livereload: true
    })
});

gulp.task('default', [
    'connect',
    'scripts_dev',
    'bower_scripts',
    'bower_css',
    'css',
    'fonts',
    'templates',
    'copy_index',
    'watch'
]);


//---------- UTILITIES ----------//

function __reload(event) {
    return gulp.src(event.path)
        .pipe(connect.reload());
}