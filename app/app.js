/**
 *
 * @Description: Ciklum test work.
 *
 * Created by Alexander Kazantsev on 9/29/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'templates',
        'main',
        'view_posts',
        'create_post',
        'info',
        'resource.provider',
        'storage.helper',
        'default_storage',
        'regexp.constants'
    ];


    angular
        .module('ciklum', __dependencies)
        .config(function ($urlRouterProvider) {
            $urlRouterProvider.otherwise('/main/posts');
        })
        .run(function (DEFAULT_STORAGE, LocalStorageHelper) {

            /**
             * Setting default data to local storage if not exist.
             */
            for (var key in DEFAULT_STORAGE) {
                var lowerKey = key.toLowerCase();
                if (!LocalStorageHelper.get(lowerKey)) {
                    console.info('ADDED DEFAULT ' + key);
                    LocalStorageHelper.set(lowerKey, DEFAULT_STORAGE[key]);
                }
            }
        });
})();