/**
 * Просто хранилище для дефолтныох значений, которые записываются в
 * localStorage при запуске приложения...
 *
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    angular
        .module('default_storage', [])
        .constant('DEFAULT_STORAGE', {
            INFO: {
                header: 'Lorem ipsum.',
                body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dicta dignissimos expedita harum ' +
                'incidunt iusto laborum libero magnam maiores maxime, molestias nam nesciunt nobis nulla obcaecati odio ' +
                'reiciendis rerum soluta vel voluptatum? Accusamus accusantium amet asperiores consectetur consequatur culpa ' +
                'cupiditate deleniti dicta dolor earum eum expedita fuga maiores maxime molestias nobis, nostrum ' +
                'odit officiis pariatur perspiciatis quasi rerum soluta tenetur veniam vitae voluptas voluptates. ' +
                'A aliquid beatae blanditiis dolore eos eum explicabo illum ipsum iste iure laborum libero nam nostrum ' +
                'numquam obcaecati officiis perferendis perspiciatis provident qui quisquam quod, repellendus ' +
                'temporibus veniam. Aperiam id impedit itaque, labore natus nemo numquam officia porro, sapiente, soluta ' +
                'unde voluptas voluptatibus! Ab distinctio dolore enim eos maxime, odio provident, quaerat quisquam quo, ' +
                'quos rerum sit voluptatem? Aut earum eos esse incidunt nobis officia reprehenderit sapiente! Accusamus, ' +
                'beatae eaque eius excepturi ipsa laudantium mollitia, numquam ratione recusandae sint totam ullam vero. ' +
                'Deserunt eligendi explicabo itaque ratione sint veritatis. Corporis, non, obcaecati. Accusamus aliquam ' +
                'aliquid aut culpa distinctio, doloremque dolorum, eos esse et ex excepturi fugit libero nemo nostrum ' +
                'officiis possimus quidem quis quo quos repellendus tempora tempore voluptate! Dolor eaque, earum eligendi ' +
                'eos exercitationem iusto laudantium minus modi molestiae nostrum reiciendis rerum ut vitae voluptatibus?'
            },
            POSTS: []
        })
})();