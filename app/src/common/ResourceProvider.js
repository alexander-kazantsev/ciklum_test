/**
 * @Description: Изначально задмувалась как аналог ngResource для того
 * чтобы можно было легко поменять и использовать с реальным бекендом,
 * но времени доделать ее не хватило, поэтому оставляю как есть.
 *
 * В принципе можно поменять в *Resource.js-файлах на стандартный $resource,
 * дописать урлы и все будет работать...
 *
 *
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = ['storage.helper'];

    function ResourceService() {
        return {
            $get: function ($q, LocalStorageHelper) {
                return {
                    /**
                     * Getting local storage data.
                     * @param {String} type
                     * @returns {promise}
                     */
                    get: function (type) {
                        var deferred = $q.defer();
                        var storageData = LocalStorageHelper.get(type);
                        deferred.resolve(storageData);
                        return deferred.promise;
                    },
                    /**
                     * Saving new post.
                     * @param {String|*} type
                     * @param {Object} data
                     * @returns {promise}
                     */
                    save: function (type, data) {
                        var deferred = $q.defer();
                        if (arguments.length === 1) {
                            data = type;
                            type = 'posts';
                        }
                        data.created_at = moment();
                        LocalStorageHelper.save(type, data);
                        deferred.resolve(this.get(type));
                        return deferred.promise;
                    }
                }
            }
        }

    }

    angular
        .module('resource.provider', __dependencies)
        .provider('_$resource', ResourceService);
})();