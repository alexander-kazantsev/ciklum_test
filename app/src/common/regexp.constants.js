/**
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    angular
        .module('regexp.constants', [])
        .constant('REG_EXP', {
            EMAIL: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/
        })
})();