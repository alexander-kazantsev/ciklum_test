/**
 *
 * @Description: Несколько методов взял из своего старого проекта.
 * Суть в том, чтобы работать с localStorage, как с обычным объектом/массивом.
 * (оставил только часть с массивом)
 *
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    function LocalStorageHelper() {
        var Storage = localStorage;
        var PREFIX = 'ciklum_';

        return {
            set: function (k, v) {
                k = (PREFIX + k);
                if (arguments.length !== 2)
                    return false;
                if (v && typeof(v) !== "string") {
                    v = JSON.stringify(v);
                }
                return Storage.setItem(k, v);
            },
            get: function (k) {
                k = (PREFIX + k);
                if (arguments.length !== 1)
                    return false;
                var result = Storage.getItem(k);
                if (result === null) {
                    return false;
                }
                return result.match(/\[.*\]|\{.*\}/)
                    ? JSON.parse(result)
                    : result;
            },
            save: function (k, v) {
                var current_value = this.get(k);
                if (_.isEmpty(current_value))
                    current_value = [];
                else if (!(current_value instanceof Array))
                    return false;
                current_value.push(v);
                return this.set(k, current_value);
            },
            clear: function (item) {
                return (arguments.length === 1 ? Storage.removeItem(PREFIX + item) : Storage.clear());
            }
        }
    }

    angular
        .module('storage.helper', [])
        .factory('LocalStorageHelper', LocalStorageHelper);
})();