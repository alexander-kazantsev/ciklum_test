/**
 *
 * Абстарктный стейт для того, чтобы не плодить navbar по всему приложению.
 *
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    var __dependencies = [];

    angular
        .module('main.routes', __dependencies)
        .config(function ($stateProvider) {
            $stateProvider
                .state('main', {
                    url: '/main',
                    controller: 'MainCtrl',
                    templateUrl: 'src/main/main.tmpl.html',
                    abstract: true
                });
        });
})();