/**
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'main.routes'
    ];

    function MainCtrl() {
    }

    angular
        .module('main', __dependencies)
        .controller('MainCtrl', MainCtrl)
})();