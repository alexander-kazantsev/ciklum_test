/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'info.routes'
    ];

    function InfoCtrl($scope, _info) {
        $scope.info = _info;
    }

    angular
        .module('info', __dependencies)
        .controller('InfoCtrl', InfoCtrl);
})();