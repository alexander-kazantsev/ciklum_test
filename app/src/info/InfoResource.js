/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'resource.provider'
    ];

    function InfoResource(_$resource) {
        /**
         * Поменять на `return $resource`
         */
        return _$resource;
    }

    angular
        .module('info.resource', __dependencies)
        .service('InfoResource', InfoResource);
})();