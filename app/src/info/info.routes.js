/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __resources = [
        'info.resource'
    ];

    angular
        .module('info.routes', __resources)
        .config(function ($stateProvider) {
            $stateProvider
                .state('info', {
                    url: '/info',
                    parent: 'main',
                    views: {
                        main: {
                            controller: 'InfoCtrl',
                            templateUrl: 'src/info/info.tmpl.html'
                        }
                    },
                    resolve: {
                        _info: function (InfoResource) {
                            return InfoResource.get('info');
                        }
                    }
                });
        })
})();