/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'posts.service'
    ];

    angular
        .module('view_posts.routes', __dependencies)
        .config(function ($stateProvider) {
            $stateProvider
                .state('view_posts', {
                    url: '/posts',
                    parent: 'main',
                    views: {
                        main: {
                            controller: 'PostsCtrl',
                            templateUrl: 'src/posts/view/view_posts.tmpl.html'
                        }
                    },
                    resolve: {
                        _posts: function (PostsService) {
                            return PostsService.get();
                        }
                    }
                });
        });
})();