/**
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    var __dependencies = ['posts.resource'];

    function PostsService(PostsResource) {

        this.get = function () {
            return PostsResource.get('posts');
        }
    }

    angular
        .module('posts.service', __dependencies)
        .service('PostsService', PostsService);
})();