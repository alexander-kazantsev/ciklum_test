/**
 * Created by Alexander Kazantsev on 10/2/15.
 */
(function () {
    'use strict';

    function postsFilter() {
        return function (input, currentPage, itemsPerPage) {

            /**
             * cleared expired posts.
             * @type {Array}
             */
            input = _.reject(input, function (value) {
                return moment().diff(moment(value.expired_at), 'day') >= 0;
            });

            /**
             * Пагинация. Просто отдает `c` `по` элементы массива.
             * Вообще лучше делать пагинацию через бекенд - http://some_api/resource?start=0&count=10.
             * Ну или как-то так.
             *
             * Не хватило времени для реализации этого через `_$resource + LocalStorageHelper`.
             */

            var start = (currentPage - 1) * itemsPerPage,
                end = start + itemsPerPage;
            return input.slice(start, end);
        };
    }

    angular
        .module('posts.filter', [])
        .filter('postsFilter', postsFilter);
})();