/**
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    var __dependencies = ['resource.provider'];

    function PostsResource(_$resource) {
        /**
         * Поменять на `return $resource`
         */
        return _$resource;
    }

    angular
        .module('posts.resource', __dependencies)
        .factory('PostsResource', PostsResource);

})();