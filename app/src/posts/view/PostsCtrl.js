/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'view_posts.routes',
        'posts.service',
        'posts.filter'
    ];

    function PostsCtrl($scope, _posts) {
        console.info('POSTS: ', _posts);

        $scope.posts = _posts;

        $scope.ITEMS_PER_PAGE = 10;
        $scope.CURRENT_PAGE = 1;
        $scope.counts = [10, 20, 50];
    }

    angular
        .module('view_posts', __dependencies)
        .controller('PostsCtrl', PostsCtrl);
})();