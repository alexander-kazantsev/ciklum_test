/**
 * Created by Alexander Kazantsev on 10/1/15.
 */
(function () {
    'use strict';

    var __dependencies = ['posts.resource'];

    function CreatePostService(PostsResource) {
        this.create = function (post) {
            return PostsResource.save(post);
        };
    }

    angular
        .module('create_post.service', __dependencies)
        .service('CreatePostService', CreatePostService);
})();