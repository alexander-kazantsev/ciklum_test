/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    angular
        .module('create_post.routes', [])
        .config(function ($stateProvider) {
            $stateProvider
                .state('create_post', {
                    url: '/create_post',
                    parent: 'main',
                    cache: false,
                    views: {
                        main: {
                            controller: 'CreatePostCtrl',
                            templateUrl: 'src/posts/create/create_post.tmpl.html'
                        }
                    }
                });
        });
})();