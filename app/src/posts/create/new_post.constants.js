/**
 * Created by Alexander Kazantsev on 10/2/15.
 */
(function () {
    'use strict';

    angular
        .module('new_post.constants', [])
        .constant('NEW_POST', {
            POST_LEN: 500,
            TITLE_LEN: 50
        });
})();