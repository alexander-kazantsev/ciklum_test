/**
 * Created by Alexander Kazantsev on 9/30/15.
 */
(function () {
    'use strict';

    var __dependencies = [
        'create_post.routes',
        'create_post.service',
        'regexp.constants',
        'new_post.constants'
    ];

    function CreatePostCtrl($scope, $state, CreatePostService, REG_EXP, NEW_POST) {

        $scope.REG_EXP = REG_EXP;
        $scope.NEW_POST = NEW_POST;

        $scope.DP_CONFIG = {
            min: moment().add(1, 'day'),
            format: 'yyyy-MMMM-dd'
        };

        $scope.create = function () {
            if ($scope.createForm.$valid) {
                CreatePostService.create($scope.post).then(function () {
                    $state.go('view_posts');
                });
            }
        };
    }

    angular
        .module('create_post', __dependencies)
        .controller('CreatePostCtrl', CreatePostCtrl);
})();