/**
 * Created by Alexander Kazantsev on 9/29/15.
 */
module.exports = {
    APP_SCRIPTS: [


        './app/src/main/main.routes.js',
        './app/src/main/**/*.js',

        './app/src/posts/view/view_posts.routes.js',
        './app/src/posts/view/**/*.js',

        './app/src/posts/create/create_post.routes.js',
        './app/src/posts/create/**/*.js',

        './app/src/info/info.routes.js',
        './app/src/info/**/*.js',

        './app/src/common/**/*.js',

        './app/app.js'
    ],
    BOWER_COMPONENTS: [
        './bower_components/angular/angular.min.js',
        './bower_components/angular-resource/angular-resource.min.js',
        './bower_components/angular-ui-router/release/angular-ui-router.min.js',
        './bower_components/angular-bootstrap/ui-bootstrap.min.js',
        './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './bower_components/lodash/lodash.min.js',
        './bower_components/moment/min/moment.min.js'
    ],
    FONTS: [
        './bower_components/bootstrap/dist/fonts/*.*'
    ],
    CSS: [
        './bower_components/bootstrap/dist/css/bootstrap.min.css'
    ]
};